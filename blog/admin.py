from django.contrib import admin
from django.db import models
from blog.models import BlogPost

class BlogPostAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    list_display = ['title', 'pub_date']   
    
admin.site.register(BlogPost, BlogPostAdmin)