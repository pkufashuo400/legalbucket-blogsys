import os
from django.contrib.auth.models import User
from django.shortcuts import render_to_response, get_object_or_404
from models import BlogPost

def home(request):
    return render_to_response('home.html', {'posts': BlogPost.objects.all()})

def blog_post_detail(request, year, month, day, slug):
    post = get_object_or_404(BlogPost, pub_date__year=year,
                             slug = slug)
    return render_to_response('blog/detail.html', {'post': post})
