from django.conf.urls import patterns, url

urlpatterns = patterns("blog.views",
    url("^(?P<year>\d{4})/(?P<month>\d{1,2})/(?P<day>\d{1,2})/(?P<slug>[-\w]+)$",
        "blog_post_detail", name="blog_post_detail"),
)