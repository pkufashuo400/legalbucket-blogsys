from django.db import models
from tinymce import models as tinymce_models
import datetime

class BlogPost(models.Model):
    title = models.CharField(max_length = 128)
    pub_date = models.DateTimeField()
    excerpt = tinymce_models.HTMLField(blank = True)
    body = tinymce_models.HTMLField()
    slug = models.SlugField(unique_for_date = 'pub_date')
    
    @models.permalink
    def get_absolute_url(self):
        return ('blog_post_detail', (), {'year':self.pub_date.strftime("%Y"),
                                         'month':self.pub_date.strftime("%m").lower(),
                                         'day':self.pub_date.strftime("%d"),
                                         'slug':self.slug},)
    
    def __unicode__(self):
        return self.title
    
    class Meta:
        ordering = ['-pub_date']