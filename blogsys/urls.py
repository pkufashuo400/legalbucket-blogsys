from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'myproject.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', 'blog.views.home', name='home'),
    (r'^blog/', include('blog.urls')),
    (r'^tinymce/', include('tinymce.urls')),
    
    url(r'^admin/', include(admin.site.urls)),
)
